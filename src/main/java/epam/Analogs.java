package epam;

public class Analogs {
    private String analog;

    public Analogs(){

    }
    public Analogs(String analog){
        this.analog=analog;
    }
    public String getAnalog() {
        return analog;
    }

    public void setAnalog(String analog) {
        this.analog = analog;
    }

    @Override
    public String toString() {
        return "Analogs{" +
                "analog='" + analog + '\'' +
                '}';
    }
}
