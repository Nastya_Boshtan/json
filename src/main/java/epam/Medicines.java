package epam;

import java.util.ArrayList;
import java.util.List;

public class Medicines {
    private String name;
    private String pharm;
    private String group;
    private Dosages dosages;
    private Package aPackage;
    private Certificate certificate;
    private List<Analogs> analogMedicine = new ArrayList<Analogs>();
    private List<Versions> versionsMedicine = new ArrayList<Versions>();

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getPharm() {
        return pharm;
    }

    public void setPharm(String pharm) {
        this.pharm = pharm;
    }
    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public void add(Analogs analog){
        analogMedicine.add(analog);
    }
    public void add(Versions versions){
        versionsMedicine.add(versions);
    }

    public Dosages getDosages() {
        return dosages;
    }

    public void setDosages(Dosages dosages) {
        this.dosages = dosages;
    }

    public Package getaPackage() {
        return aPackage;
    }

    public void setaPackage(Package aPackage) {
        this.aPackage = aPackage;
    }
    public Certificate getCertificate() {
        return certificate;
    }

    public void setCertificate(Certificate certificate) {
        this.certificate = certificate;
    }
    public List<Analogs> getAnalogMedicine() {
        return analogMedicine;
    }

    public void setAnalogMedicine(List<Analogs> analogMedicine) {
        this.analogMedicine = analogMedicine;
    }
    public List<Versions> getVersionsMedicine() {
        return versionsMedicine;
    }

    public void setVersionsMedicine(List<Versions> versionsMedicine) {
        this.versionsMedicine = versionsMedicine;
    }

    @Override
    public String toString() {
        return "Medicines{" +
                "name='" + name + '\'' +
                ", pharm='" + pharm + '\'' +
                ", group='" + group + '\'' +
                ", dosages=" + dosages +
                ", aPackage=" + aPackage +
                ", certificate=" + certificate +
                ", analogMedicine=" + analogMedicine +
                ", versionsMedicine=" + versionsMedicine +
                '}';
    }
}
