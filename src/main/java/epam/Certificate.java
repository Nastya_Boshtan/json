package epam;

public class Certificate {

    private String number;
    private String dateOfIssue;
    private String endDate;
    private String organisation;

    public Certificate(){

    }
    public Certificate(String number,String dateOfIssue, String endDate, String organisation){
        this.number=number;
        this.dateOfIssue=dateOfIssue;
        this.endDate=endDate;
        this.organisation=organisation;
    }
    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getDateOfIssue() {
        return dateOfIssue;
    }

    public void setDateOfIssue(String dateOfIssue) {
        this.dateOfIssue = dateOfIssue;
    }
    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }
    public String getOrganisation() {
        return organisation;
    }

    public void setOrganisation(String organisation) {
        this.organisation = organisation;
    }
    @Override
    public String toString() {
        return "Certificate{" +
                "number='" + number + '\'' +
                ", dateOfIssue='" + dateOfIssue + '\'' +
                ", endDate='" + endDate + '\'' +
                ", organisation='" + organisation + '\'' +
                '}';
    }
}
