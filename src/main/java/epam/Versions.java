package epam;

public class Versions {
    private String version;

    public Versions(){

    }
    public Versions(String version) {
        this.version = version;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    @Override
    public String toString() {
        return "Versions{" +
                "version='" + version + '\'' +
                '}';
    }

}
