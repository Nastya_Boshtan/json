package epam;

public class Dosages {
    private double dosage;
    private String periodicity;

    public Dosages(){}
    public Dosages(double dosage, String periodicity){
        this.dosage=dosage;
        this.periodicity=periodicity;
    }
    public double getDosage() {
        return dosage;
    }

    public void setDosage(double dosage) {
        this.dosage = dosage;
    }
    public String getPeriodicity() {
        return periodicity;
    }

    public void setPeriodicity(String periodicity) {
        this.periodicity = periodicity;
    }


    @Override
    public String toString() {
        return "Dosages{" +
                "dosage=" + dosage +
                ", periodicity='" + periodicity + '\'' +
                '}';
    }
}
