package epam;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;


import java.io.*;
import java.util.ArrayList;
import java.util.List;

import static com.sun.activation.registries.LogSupport.log;

public class App {

    public static void main(String[] args) throws Exception {

//        String jsonfile ="resources\\medicine.json";
//
//        //Gson gson= new Gson();
//        FileReader reader = null;
//        JSONObject jsb = null;
//        JSONParser parser = new JSONParser();
//        try {
//            reader = new FileReader(jsonfile);
//            JSONParser jsonParser = new JSONParser();
//            jsb = (JSONObject)jsonParser.parse(reader);
//
//            System.out.println(jsb);
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            if (reader != null)
//                reader.close();
//        }
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        try(Reader reader = new FileReader("resources\\medicine.json")) {

            Medicines medicines=gson.fromJson(reader,Medicines.class);
            System.out.println(medicines);
        }catch (IOException e){
            e.printStackTrace();
        }

    }
    }

