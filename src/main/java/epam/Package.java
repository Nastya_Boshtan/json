package epam;

public class Package {
    private String type;
    private  int quantity;
    private double price;

    public Package(){}
    public Package(String type, int quantity,double price){
        this.type=type;
        this.quantity=quantity;
        this.price=price;
    }
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
    @Override
    public String toString() {
        return "Package{" +
                "type='" + type + '\'' +
                ", quantity=" + quantity +
                ", price=" + price +
                '}';
    }
}
